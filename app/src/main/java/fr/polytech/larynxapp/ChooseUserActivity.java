package fr.polytech.larynxapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import fr.polytech.larynxapp.controller.home.HomeFragment;
import fr.polytech.larynxapp.model.Record;
import fr.polytech.larynxapp.model.User;
import fr.polytech.larynxapp.model.database.DBManager;

/**
 * Choose User page of the app,
 * including a list of user to choose and a button of create a new user
 * Registered in AndroidManifest.xml -> activity
 */
public class ChooseUserActivity extends AppCompatActivity {

    /**
     * button of a new User creation
     */
    private Button createButton;

    /**
     * button of entering the App
     */
    private Button startButton;

    /**
     * list of patients/user
     */
    private ListView userListView;

    /**
     * The list of user/patient data
     */
    private List<User> users;

    /**
     * the patient id who was chosen, if userId = -1, that means no patient was chosen
     */
    private int userId = -1;

    private DBManager db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_user);

        // Remove the bottom navigation area
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        db = new DBManager(getBaseContext());

        // init users map
        initUserMap();

        // initialisation of userListView, to show patients
        userListView = (ListView) findViewById(R.id.users_list_view);

        final ArrayAdapter<User> adapter = new ArrayAdapter<>(this.getApplicationContext(),
                android.R.layout.simple_list_item_1, users);
        userListView.setAdapter(adapter);

        // when the doctor selects a patient, record the patient’s id
        userListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {                     //Sets the action on a line click
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                userId = ((User)userListView.getItemAtPosition(position)).getUserId();
            }
        });

        // create new user
        createButton = (Button) findViewById(R.id.btn_create_user);
        createButton.setOnClickListener(new View.OnClickListener() {
            EditText editText;
            @Override
            public void onClick(View view) {
                final Builder builder=new Builder(ChooseUserActivity.this);
                // A dialog box for entering username
                // Builder builder=new Builder(ChooseUserActivity.this);
                builder.setTitle("Créer un nouvel utilisateur");
                editText = new EditText(ChooseUserActivity.this);
                builder.setView(editText);

                builder.setPositiveButton("Valider", new OnClickListener(){
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String input = editText.getText().toString();
                        // UserName can't be null
                        if (input.equals("")) {
                            Toast.makeText(getApplicationContext(), "Le contenu d'entrée ne peut pas être vide!" + input, Toast.LENGTH_LONG).show();
                        }
                        else {
                            // if the user name is not repeated, create this new user
                            if (!db.existUser(input)) {
                                User user = new User(input);
                                db.addUser(user);
                                users.add(db.getUsers().get(db.getUsers().size()-1));

                            }else{
                                Toast.makeText(getApplicationContext(), "L'utilisateur" + input + " existe déjà" , Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
                builder.setNegativeButton("Annuler", null);
                builder.show();
            }
        });


        // Set click action: turn to Main Activity
        startButton = (Button) findViewById(R.id.btn_enter);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (userId != -1) {
                    createFragments();
                }
            }
        });

    }

    /**
     * to create those fragments
     */
    public void createFragments(){
        setContentView(R.layout.activity_main);
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_history, R.id.navigation_evolution)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
    }


    /**
     *  Initialisation of the user's map
     */
    private void initUserMap(){
        // TODO: should be changed to DBManger()

        users = db.getUsers();
    }

    /**
     * for those fragments to get user id
     * @return userId
     */
    public int getLocalUserId(){
        return userId;
    }
    public DBManager getDB() {return db;}

}
