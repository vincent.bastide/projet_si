package fr.polytech.larynxapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;

/**
 * Welcome page of the app, including a start button
 * Register in AndroidManifest.xml -> activity
 */
public class WelcomeActivity extends Activity {

    /**
     * button of start MainActivity
     */
    private Button startButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);
        // Remove the bottom navigation area
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // Set icon picture
        ImageView icon_logo = findViewById(R.id.icon_logo);
        icon_logo.setBackgroundResource(R.mipmap.icon);

        // Get startButton from xml
        // Set click action: turn to Main Activity
        startButton = (Button) findViewById(R.id.btn_start);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Wake up ChooseUserActivity and end this activity.
                Intent mainIntent = new Intent(WelcomeActivity.this, ChooseUserActivity.class);
                startActivity(mainIntent);
                WelcomeActivity.this.finish();
            }
        });
    }

}
