package fr.polytech.larynxapp.controller.history;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.io.TarsosDSPAudioFormat;
import be.tarsos.dsp.io.UniversalAudioInputStream;
import be.tarsos.dsp.io.android.AndroidAudioPlayer;
import be.tarsos.dsp.io.android.AndroidFFMPEGLocator;
import be.tarsos.dsp.io.android.AudioDispatcherFactory;
import be.tarsos.dsp.pitch.PitchDetectionHandler;
import be.tarsos.dsp.pitch.PitchDetectionResult;
import fr.polytech.larynxapp.ChooseUserActivity;
import fr.polytech.larynxapp.R;
import fr.polytech.larynxapp.controller.home.Status_mic;
import fr.polytech.larynxapp.model.Record;
import fr.polytech.larynxapp.model.analysis.FeaturesCalculator;
import fr.polytech.larynxapp.model.analysis.PitchProcessor;
import fr.polytech.larynxapp.model.analysis.Yin;
import fr.polytech.larynxapp.model.audio.AudioData;
import fr.polytech.larynxapp.model.database.DBManager;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * The fragment of history
 * to show every record of the user
 */
public class HistoryFragment extends Fragment {

    /**
     * The line chart where the data will be shown
     */
    private LineChart mpLineChart;

    /**
     * The UI list of the data
     */
    private ListView listview;

    /**
     * The list of record datas
     */
    private List<Record> records;

    /**
     * Get Color Primary from resource XML
     */
    private int colorPrimary = Color.argb(255, 183, 88 ,58);

    /**
     * Button to import a recording file
     */
    private Button importButton;

    /**
     * Shimmer of the recording file
     */
    private double shimmer;

    /**
     * Jitter of the recording file
     */
    private double jitter;

    /**
     * f0 of the recording file
     */
    private double f0;

    /**
     * The id of the current user
     */
    private int userId;

    private int copy(){
        return 0;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            System.out.println(uri.getPath());
            String FILE_PATH = Environment.getExternalStorageDirectory().getPath() + File.separator + "voiceRecords";
            String[] p = uri.getPath().split("/");

            DateFormat dateFormat  = new SimpleDateFormat( "dd-MM-yyyy HH-mm-ss" );
            Date currentDate = new Date( System.currentTimeMillis() );
            String FILE_NAME = dateFormat.format( currentDate );
            String fullFile = FILE_PATH + File.separator + FILE_NAME + ".wav";
            System.out.println(fullFile);
            try{
                InputStream fosfrom = new FileInputStream(uri.getPath().split(":")[1]);
                OutputStream fosto = new FileOutputStream(fullFile);
                byte bt[] = new byte[1024];
                int c;
                while ((c = fosfrom.read(bt)) > 0)
                {
                    fosto.write(bt, 0, c);
                }
                fosfrom.close();
                fosto.close();

                Record record = new Record(FILE_NAME, fullFile, userId);
                DBManager manager = new DBManager(getContext());
                manager.add(record);
                records.add(record);
                analyseRecord(fullFile);
                manager.updateRecordVoiceFeatures(FILE_NAME, jitter, shimmer, f0);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Analyse the record from file
     *
     * @param finalPath the path of the file
     */
    public void analyseRecord(String finalPath) throws FileNotFoundException {
        AudioData audioData = new AudioData();
        boolean fileOK;

        File file = new File(finalPath);
        try {
            if (!file.exists())
                //noinspection ResultOfMethodCallIgnored we don't need the result because we try to create only if the file doesn't exist.
                file.createNewFile();

            fileOK = true;
        }
        catch (IOException e) {
            Log.e("AnalyseData", e.getMessage(), e);
            fileOK = false;
        }

        if (!fileOK) {
            return;
        }

        try {
            FileInputStream inputStream = new FileInputStream(file);
            byte[] b = new byte[inputStream.available()];
            inputStream.read(b);
            short[] s = new short[(b.length - 44) / 2];
            ByteBuffer.wrap(b)
                    .order(ByteOrder.LITTLE_ENDIAN)
                    .asShortBuffer()
                    .get(s);

            for (short ss : s) {
                audioData.addData(ss);
            }

            audioData.setMaxAmplitudeAbs();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        audioData.processData();

        final List<Float> pitches;
        pitches = new ArrayList<>();

        //Convert into TarsosDSP API
        InputStream is = new FileInputStream(file);
        TarsosDSPAudioFormat audioFormat = new TarsosDSPAudioFormat(
                /* sample rate */ 44100,
                /* HERE sample size in bits */ 16,
                /* number of channels */ 1,
                /* signed/unsigned data */ true,
                /* big-endian byte order */ false
        );
        AudioDispatcher dispatcher = new AudioDispatcher(new UniversalAudioInputStream(is, audioFormat), 2048, 0);
        PitchDetectionHandler pitchDetectionHandler = new PitchDetectionHandler() {
            @Override
            public void handlePitch(PitchDetectionResult res, AudioEvent e){

                float pitchInHz = res.getPitch();
                if(pitchInHz != -1 && pitchInHz < 400)
                    pitches.add(pitchInHz);
        }
        };
        AudioProcessor p = new PitchProcessor(new Yin(44100, 2048), 44100, 2048, pitchDetectionHandler);
        dispatcher.addAudioProcessor(p);
        dispatcher.run();



        FeaturesCalculator featuresCalculator = new FeaturesCalculator(audioData, pitches);

        shimmer = featuresCalculator.getShimmer() * 100;
        jitter = featuresCalculator.getJitter() * 100;
        f0 = featuresCalculator.getfundamentalFreq();
        Log.e("Jitter",jitter+"");
        Log.e("Shimmer",shimmer+"");
        Log.e("f0",f0+"");
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        // to get current user id
        userId = ((ChooseUserActivity)getActivity()).getLocalUserId();
        View root = inflater.inflate(R.layout.fragment_history, container, false);      //Sets the view for the fragment
        initMap();
        importButton = root.findViewById(R.id.btn_importer);
        importButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("audio/*");
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                startActivityForResult(intent, 1);
            }
        });

        //********************************Creation of the line chart*******************************/
        mpLineChart = root.findViewById(R.id.line_chart);

        XAxis xAxis = mpLineChart.getXAxis();
        xAxis.setDrawGridLines(false);
        YAxis yAxis = mpLineChart.getAxisLeft();
        yAxis.setDrawGridLines(false);

        final ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        if(!records.isEmpty() ){
            LineDataSet lineDataSet = new LineDataSet(dataValues(records.get(0)), records.get(0).getName());
            setLineData(lineDataSet);
            dataSets.add((lineDataSet));
            final LineData data = new LineData(dataSets);
            mpLineChart.setData(data);
        }

        //***********************************Creation of the list**********************************/
        listview = root.findViewById(R.id.listViewRecords);
        final ArrayAdapter<Record> adapter = new ArrayAdapter<>(getActivity().getApplicationContext(),
                android.R.layout.simple_list_item_1, records);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {                     //Sets the action on a line click
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dataSets.clear();
                LineDataSet tmpLineDataSet = new LineDataSet(dataValues(records.get(position)), records.get(position).getName());
                setLineData(tmpLineDataSet);
                dataSets.add(tmpLineDataSet);
                final LineData data = new LineData(dataSets);
                mpLineChart.setData(data);
                mpLineChart.invalidate();
            }
        });

        setChart(mpLineChart);
        mpLineChart.setDrawGridBackground(false);
        mpLineChart.invalidate();

        return root;
    }

    /**
     * Sets the data set's graphical parameters
     * @param lineDataSet the data set to configure
     */
    private void setLineData(LineDataSet lineDataSet){
        lineDataSet.setCircleColor(colorPrimary); // Yuman: change circle color
        lineDataSet.setCircleRadius(5f);
        lineDataSet.setCircleHoleRadius(0);
        lineDataSet.setValueTextSize(0f);
    }

    /**
     * Sets the data that will be shown in the chart
     * @param recordIn the record that contain shimmer and jitter data
     * @return the array list that will be shown
     */
    private ArrayList<Entry> dataValues(Record recordIn){
        ArrayList<Entry> dataVals = new ArrayList<>();
        dataVals.add(new Entry((float)recordIn.getJitter(), (float)recordIn.getShimmer()));
        return dataVals;
    }

    /**
     *  Initialisation of the data's map
     */
    private void initMap(){
        records = new DBManager(getContext()).query(userId);
    }

    /**
     * Set the graphic feature of the line chart
     * @param chart the chart to be set
     */
    private void setChart(LineChart chart){

        //The line chart's y axis
        YAxis yAxis = chart.getAxisLeft();

        //The line chart's x axis
        XAxis xAxis = chart.getXAxis();

        //Disable the right axis
        chart.getAxisRight().setEnabled(false);

        //Set the y axis property
        yAxis.setAxisLineWidth(2.5f);
        yAxis.setAxisLineColor(Color.GRAY);
        yAxis.setAxisMinimum(0f);
        yAxis.setAxisMaximum(10f);
        yAxis.setTextSize(12f);

        //Set the x axis property
        xAxis.setAxisLineWidth(2f);
        xAxis.setAxisLineColor(Color.GRAY);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setAxisMinimum(0f);
        xAxis.setAxisMaximum(3f);
        xAxis.setTextSize(12f);

        chart.getLegend().setEnabled(false);
        chart.getDescription().setEnabled(false);
    }
}
