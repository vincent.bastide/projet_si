package fr.polytech.larynxapp.controller.evolution;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import fr.polytech.larynxapp.ChooseUserActivity;
import fr.polytech.larynxapp.R;
import fr.polytech.larynxapp.model.Record;
import fr.polytech.larynxapp.model.database.DBManager;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * The fragment of evolution
 * To show the changing trend of shimmer and gitter
 */
public class EvolutionFragment extends Fragment {

    /**
     * The line chart where the shimmer values will be shown
     */
    private LineChart shimmerMpLineChart;

    /**
     * The line chart where the jitter values will be shown
     */
    private LineChart jitterMpLineChart;

    /**
     * The list of record datas
     */
    private List<Record> records;

    /**
     * The startDate Button
     */
    private ImageButton startDateButton;


    /**
     * The endDate Button
     */
    private ImageButton endDateButton;

    /**
     * The start date
     */
    private int startDateDay;
    private int startDateMonth;
    private int startDateYear;

    /**
     * The end date
     */
    private int endDateDay;
    private int endDateMonth;
    private int endDateYear;

    /**
     * The text of start date
     */
    private TextView startDateText;

    /**
     * The text of end date
     */
    private TextView endDateText;

    /**
     * Get Color Primary from resource XML
     */
    private int colorPrimary = Color.argb(255, 183, 88 ,58);
    private int colorLimitRed = Color.argb(255, 186, 110, 110);

    /**
     * The Suivi View, it could be used/repaint in different methods
     */
    private View root;

    /**
     * Button of resetting date
     */
    private ImageButton resetDateButton;

    /**
     * The current user id
     */
    private int userId;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        // Sets the view the the fragment
        root = inflater.inflate(R.layout.fragment_evolution, container, false);

        userId = ((ChooseUserActivity)getActivity()).getLocalUserId();
        records = new DBManager(getContext()).query(userId);

        startDateButton = root.findViewById(R.id.startDate);
        endDateButton = root.findViewById(R.id.endDate);

        // Start time, end time and reset date button
        startDateText = root.findViewById(R.id.startDateDescription);
        endDateText = root.findViewById(R.id.endDateDescription);
        resetDateButton = root.findViewById(R.id.resetDate);

        initDateButton();
        drawShimmer();
        drawJitter();

        return root;
    }

    /**
     * draw shimmer scheme
     * Create of the shimmer's chart
     */
    private void drawShimmer(){
        final TextView shimmerTextView = root.findViewById(R.id.shimmer_text_view);
        shimmerTextView.setText("Shimmer");
        shimmerTextView.setTextSize(20f);
        shimmerTextView.setTextColor(Color.BLACK);


        shimmerMpLineChart = root.findViewById(R.id.shimmer_line_chart);
        setShimmerChart(shimmerMpLineChart);

        LineDataSet shimmerLineSet = new LineDataSet(shimmerDataValues(), "Shimmer");
        shimmerLineSet.setColor(Color.BLACK);
        shimmerLineSet.setLineWidth(2f);

        // circleColor
        shimmerLineSet.setCircleColor(colorPrimary);
        shimmerLineSet.setCircleRadius(5f);
        shimmerLineSet.setCircleHoleRadius(2.5f);
        shimmerLineSet.setValueTextSize(0f);
        ArrayList<ILineDataSet> shimmerDataSets = new ArrayList<>();
        shimmerDataSets.add((shimmerLineSet));

        LimitLine shimmerLl = new LimitLine(3.8f);
        shimmerLl.setLabel("Limite shimmer");
        shimmerLl.setTextColor(colorLimitRed);
        shimmerLl.setLineColor(colorLimitRed);

        // Set dashed line
        shimmerLl.enableDashedLine(10f,10f,0);
        shimmerMpLineChart.getAxisLeft().addLimitLine(shimmerLl);

        XAxis shimmerXAxis = shimmerMpLineChart.getXAxis();

        // Delete vertical grid
        shimmerXAxis.setDrawGridLines(false);
        shimmerXAxis.setGranularity(1f);
        shimmerXAxis.setSpaceMax(0.1f);
        shimmerXAxis.setSpaceMin(0.1f);

        shimmerXAxis.setValueFormatter(new com.github.mikephil.charting.formatter.IndexAxisValueFormatter(dateValues(startDateText.getText().toString(), endDateText.getText().toString())));

        YAxis shimmerLeftAxis = shimmerMpLineChart.getAxisLeft();

        // Delete horizontal grid
        shimmerLeftAxis.setDrawGridLines(false);

        LineData shimmerData = new LineData(shimmerDataSets);
        shimmerMpLineChart.setData(shimmerData);
        shimmerMpLineChart.invalidate();
        shimmerMpLineChart.setDrawGridBackground(false);
    }

    /**
     * draw jitter scheme
     * create the jitter's chart
     */
    private void drawJitter(){
        final TextView jitterTextView = root.findViewById(R.id.jitter_text_view);
        jitterTextView.setText("Jitter");
        jitterTextView.setTextSize(20f);
        jitterTextView.setTextColor(Color.BLACK);

        jitterMpLineChart = root.findViewById(R.id.jitter_line_chart);
        setJitterChart(jitterMpLineChart);

        LineDataSet jitterLineSet = new LineDataSet(jitterDataValues(), "Jitter");
        jitterLineSet.setColor(Color.BLACK);
        jitterLineSet.setLineWidth(2f);
        jitterLineSet.setCircleColor(colorPrimary);
        jitterLineSet.setCircleRadius(5f);
        jitterLineSet.setCircleHoleRadius(2.5f);
        jitterLineSet.setValueTextSize(0f);
        ArrayList<ILineDataSet> jitterDataSets = new ArrayList<>();
        jitterDataSets.add((jitterLineSet));

        LimitLine jitterLl = new LimitLine(2.04f);
        jitterLl.setLabel("Limite jitter");
        jitterLl.setTextColor(colorLimitRed);
        jitterLl.setLineColor(colorLimitRed);

        // Set dashed line
        jitterLl.enableDashedLine(10f,10f,0);
        jitterMpLineChart.getAxisLeft().addLimitLine(jitterLl);

        XAxis jitterXAxis = jitterMpLineChart.getXAxis();

        // Delete vertical grid
        jitterXAxis.setDrawGridLines(false);
        jitterXAxis.setGranularity(1f);
        jitterXAxis.setSpaceMax(0.1f);
        jitterXAxis.setSpaceMin(0.1f);
        jitterXAxis.setValueFormatter(new com.github.mikephil.charting.formatter.IndexAxisValueFormatter(dateValues(startDateText.getText().toString(), endDateText.getText().toString())));

        YAxis jitterLeftAxis = jitterMpLineChart.getAxisLeft();

        // Delete horizontal grid
        jitterLeftAxis.setDrawGridLines(false);

        LineData jitterData = new LineData(jitterDataSets);
        jitterMpLineChart.setData(jitterData);
        jitterMpLineChart.invalidate();
        jitterMpLineChart.setDrawGridBackground(false);
    }

    /**
     * Initialisation of the shimmer data's arraylist
     * @return the shimmer data's arraylist
     */
    private ArrayList<Entry> shimmerDataValues(){
        ArrayList<Entry> dataVals = new ArrayList<>();
        int addI = 0;
        String startDate = startDateText.getText().toString();
        String endDate = endDateText.getText().toString();

        // In order to ensure uniform data format,
        // it is necessary to convert "début" and "fin" into date format
        if (startDate.equals("Début"))
            startDate = "0/0/0";
        if (endDate.equals("Fin"))
            endDate = "32/13/9999";
        String[] startTime = startDate.split("/");
        String[] endTime = endDate.split("/");

        for(int i = 0; i < records.size(); i++) {
            String[] dateTimes = records.get(i).getName().split("-");

            // Choose the recording in the start and end dates
            // If the recording is between start and end dates, add it into dataVals
            if (((Integer.parseInt(startTime[1]) < Integer.parseInt(dateTimes[1]))
                    || ((Integer.parseInt(startTime[1]) == Integer.parseInt(dateTimes[1])) && (Integer.parseInt(startTime[0]) <= Integer.parseInt(dateTimes[0]))))
                    && ((Integer.parseInt(endTime[1]) > Integer.parseInt(dateTimes[1]))
                    || ((Integer.parseInt(endTime[1]) == Integer.parseInt(dateTimes[1])) && (Integer.parseInt(endTime[0]) >= Integer.parseInt(dateTimes[0]))))) {
                dataVals.add(new Entry(addI, (float) records.get(i).getShimmer()));
                addI += 1;
            }
        }
        return dataVals;
    }

    /**
     * Initialisation of the jitter data's arraylist
     * @return the jitter data's arraylist
     */
    private ArrayList<Entry> jitterDataValues(){
        ArrayList<Entry> dataVals = new ArrayList<>();
        int addI = 0;
        String startDate = startDateText.getText().toString();
        String endDate = endDateText.getText().toString();
        if (startDate.equals("Début"))
            startDate = "0/0/0";
        if (endDate.equals("Fin"))
            endDate = "32/13/9999";
        String[] startTime = startDate.split("/");
        String[] endTime = endDate.split("/");
        for(int i = 0; i < records.size(); i++) {
            String[] dateTimes = records.get(i).getName().split("-");
            if (((Integer.parseInt(startTime[1]) < Integer.parseInt(dateTimes[1]))
                    || ((Integer.parseInt(startTime[1]) == Integer.parseInt(dateTimes[1])) && (Integer.parseInt(startTime[0]) <= Integer.parseInt(dateTimes[0]))))
                    && ((Integer.parseInt(endTime[1]) > Integer.parseInt(dateTimes[1]))
                    || ((Integer.parseInt(endTime[1]) == Integer.parseInt(dateTimes[1])) && (Integer.parseInt(endTime[0]) >= Integer.parseInt(dateTimes[0]))))) {
                dataVals.add(new Entry(i, (float) records.get(i).getJitter()));
                addI += 1;
            }
        }
        return dataVals;
    }

    /**
     * Initialisation of the dates arraylist
     * @return the dates arraylist
     */
    private String[] dateValues(String startDate, String endDate){
        ArrayList<String> dates = new ArrayList<>();
        int addI = 0;
        for(int i = 0; i < records.size(); i++)
        {
            String strippedName = records.get(i).getName().replace("-", " ");
            String[] dateTimes = strippedName.split(" ");
            if (startDate.equals("Début"))
                startDate = "0/0/0";
            if (endDate.equals("Fin"))
                endDate = "32/13/9999";
            String[] startTime = startDate.split("/");
            String[] endTime = endDate.split("/");
            if (((Integer.parseInt(startTime[1]) < Integer.parseInt(dateTimes[1]))
                    || ((Integer.parseInt(startTime[1]) == Integer.parseInt(dateTimes[1])) && (Integer.parseInt(startTime[0]) <= Integer.parseInt(dateTimes[0]))))
            && ((Integer.parseInt(endTime[1]) > Integer.parseInt(dateTimes[1]))
                    || ((Integer.parseInt(endTime[1]) == Integer.parseInt(dateTimes[1])) && (Integer.parseInt(endTime[0]) >= Integer.parseInt(dateTimes[0]))))) {
                //dates.add(i ,dateTimes[0] + "-" + dateTimes[1] + "-" + dateTimes[2]); // Yuman
                dates.add(addI, dateTimes[0] + "-" + dateTimes[1]); //Yuman
                addI += 1;
            }
        }
        return dates.toArray(new String[0]);
    }

    /**
     * Set the graphic feature of the line charts for the shimmer
     * @param chart the chart to be set
     */
    private void setShimmerChart(LineChart chart){

        //The line chart's y axis
        YAxis yAxis = chart.getAxisLeft();

        //The line chart's x axis
        XAxis xAxis = chart.getXAxis();

        //Disable the right axis
        chart.getAxisRight().setEnabled(false);

        //Set the y axis property
        yAxis.setAxisLineWidth(2f);
        yAxis.setAxisLineColor(Color.BLACK);
        yAxis.setAxisMinimum(0f);
        yAxis.setAxisMaximum(10f);
        yAxis.setTextSize(12f);

        //Set the x axis property
        xAxis.setAxisLineWidth(2f);
        xAxis.setAxisLineColor(Color.BLACK);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(12f);

        chart.getLegend().setEnabled(false);
        chart.getDescription().setEnabled(false);
        chart.setScaleEnabled(true);
        chart.setTouchEnabled(false);
    }

    /**
     * Set the graphic feature of the line charts for the jitter
     * @param chart the chart to be set
     */
    private void setJitterChart(LineChart chart){

        //The line chart's y axis
        YAxis yAxis = chart.getAxisLeft();

        //The line chart's x axis
        XAxis xAxis = chart.getXAxis();

        //Disable the right axis
        chart.getAxisRight().setEnabled(false);

        //Set the y axis property
        yAxis.setAxisLineWidth(2f);
        yAxis.setAxisLineColor(Color.GRAY);
        yAxis.setAxisMinimum(0f);
        yAxis.setAxisMaximum(6f);
        yAxis.setTextSize(12f);

        //Set the x axis property
        xAxis.setAxisLineWidth(2f);
        xAxis.setAxisLineColor(Color.GRAY);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(12f);

        chart.getLegend().setEnabled(false);
        chart.getDescription().setEnabled(false);

        chart.setScaleEnabled(true);
        chart.setTouchEnabled(false);
    }

    /**
     * Response procedures for the three date-related buttons
     */
    private void initDateButton() {
        // Reset date button,
        // the start date and end date become the original state,
        // redraw the shimmer and jitter line graphs.
        resetDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                startDateText.setText("Début");
                endDateText.setText("Fin");
                drawShimmer();
                drawJitter();
            }
        });

        // Select the start date,
        // display it on the page, and redraw the line chart.
        startDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                startDateYear = year;
                                startDateMonth = month + 1;
                                startDateDay = day;
                                startDateText.setText(Integer.toString(startDateDay) + '/' + Integer.toString(startDateMonth) + '/' + Integer.toString(startDateYear));
                                drawShimmer();
                                drawJitter();
                            }
                        }, year, month, dayOfMonth);
                datePickerDialog.show();
            }
        });

        // Select the end date,
        // display it on the page, and redraw the line chart.
        endDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                                endDateYear = year;
                                endDateMonth = month + 1;
                                endDateDay = day;
                                endDateText.setText(Integer.toString(endDateDay) + '/' + Integer.toString(endDateMonth) + '/' + Integer.toString(endDateYear));
                                drawShimmer();
                                drawJitter();
                            }
                        }, year, month, dayOfMonth);
                datePickerDialog.show();
            }
        });
    }
}